# Grossary

## Technology

### jsonnet
- reference: https://jsonnet.org/
- Add programability to json to specify json
- syntax similar to jq?
- e.g.
```json
{
  person1: {
    name: "Alice",
    welcome: "Hello " + self.name + "!",
  },
  person2: self.person1 { name: "Bob" },

  ```


